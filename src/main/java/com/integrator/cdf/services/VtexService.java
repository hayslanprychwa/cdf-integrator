package com.integrator.cdf.services;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class VtexService {

    @Autowired
    private RestTemplate restTemplate;

    String URL = "https://carrefourbr.vtexcommercestable.com.br/api/oms/pvt/orders/";
    String APP_KEY = "vtexappkey-carrefourbrqa-RERURO";
    String APP_TOKEN = "HWMWWAHORLEKTGJTVGYKOHUTALPTHZMAAGQPPFVNNTXGLKHGOKNDUGHACTWKKOHRYEKNPTEQLKXVJNHQEJFWMJFTYQMWJKDYLFHGATQUGXRJJMQOLGFAIKFOYQOYGIGA";

    public JsonNode orderIntegration(String orderId){

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("X-VTEX-API-AppKey", APP_KEY);
        headers.set("X-VTEX-API-AppToken", APP_TOKEN);

        HttpEntity<String> request = new HttpEntity<>(headers);
        JsonNode response = restTemplate.exchange(URL+orderId, HttpMethod.GET, request, JsonNode.class).getBody();
        return response;
    }
}
