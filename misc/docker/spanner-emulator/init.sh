#!/bin/bash

main(){
  echo "Creating docker spanner-emulator"
  docker run --name spanner-emulator -d --rm  -p 9010:9010 -p 9020:9020 gcr.io/cloud-spanner-emulator/emulator

  gcloud config set auth/disable_credentials true
  gcloud config set project cdf-integrator
  gcloud config set api_endpoint_overrides/spanner http://localhost:9020/

  gcloud spanner instances create cdf-integrator-instance --config=regional-us-east1 --description='CDF Integrator Instance' --nodes=1
  gcloud spanner databases create cdf-integrator --instance=cdf-integrator-instance
  gcloud config configurations activate emulator

  if isFromRootFolder;
  then
      if [ "$1" == "windows" ];
      then
        echo "Running Init DDL for Windows environment"
        ./misc/docker/spanner-emulator/runAllDDL.sh "windows"
      else
        ./misc/docker/spanner-emulator/runAllDDL.sh
      fi
    if [[ $? == 0 ]]
    then
      ./misc/docker/spanner-emulator/runAllDML.sh
    fi
  else
    ./runAllDDL.sh
    if [[ $? == 0 ]]
    then
      ./runAllDML.sh
    fi
  fi
}

isFromRootFolder(){
  if [[ $(pwd) == *"/misc/docker"* ]];
  then
    return 1 #false
  else
    return 0 #true
  fi
}

main "$@";
