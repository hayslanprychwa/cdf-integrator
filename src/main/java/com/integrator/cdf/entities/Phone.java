package com.integrator.cdf.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "phone")
@Getter
@Setter
public class Phone {

    public Phone() {
    }

    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    UUID id;

    @Column
    Integer areaCode;

    @Column
    String phone;

    @Column(name = "NR_RAMAL")
    Integer NR_RAMAL;

    @Column
    String receiverName;

    @JsonIgnore
    @ManyToMany(mappedBy = "phoneList")
    Set<Order> orders;

    public Phone(Integer areaCode, String phone, Integer NR_RAMAL, String receiverName) {
        this.areaCode = areaCode;
        this.phone = phone;
        this.NR_RAMAL = NR_RAMAL;
        this.receiverName = receiverName;
    }
}
