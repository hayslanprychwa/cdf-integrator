package com.integrator.cdf.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "email")
@Getter
@Setter
public class Email {

    public Email() {
    }

    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    UUID id;

    @Column
    String email;

    @JsonIgnore
    @ManyToMany(mappedBy = "emailList")
    Set<Order> orders;

    public Email(String email) {
        this.email = email;
    }
}
