package com.integrator.cdf.repository;

import com.integrator.cdf.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.UUID;

@Repository
public interface OrderRepository extends JpaRepository<Order, UUID> {

    @Query(value = "SELECT * FROM ORDERS o where o.order_id = :orderId ", nativeQuery = true)
    Order findByOrderId(String orderId);

    Order findByOrderSeq(Integer orderSeq);
    ArrayList<Order> findByOrderStatus(String status);
    ArrayList<Order> findByDocument(Long document);
    ArrayList<Order> findByEmailList_Email(String email);
    ArrayList<Order> findByPhoneList_Phone(String phone);
    ArrayList<Order> findByCreationDate(Date creationDate);

    @Query(value = "SELECT * FROM ORDERS o where o.creation_date BETWEEN :startDate AND :endDate", nativeQuery = true)
    ArrayList<Order> findByCreationDateStartAndEnd(Date startDate, Date endDate);

}
