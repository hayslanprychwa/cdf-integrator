package com.integrator.cdf.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "address")
@Getter
@Setter
public class Address {

    public Address() {
    }

    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    UUID id;

    @Column
    String street;

    @Column
    Integer number;

    @Column
    Integer postalCode;

    @Column
    String neighborhood;

    @Column
    String city;

    @Column
    String state;

    @JsonIgnore
    @ManyToMany(mappedBy = "addressList")
    Set<Order> orders;

    public Address(String street, Integer number, Integer postalCode, String neighborhood, String city, String state) {
        this.street = street;
        this.number = number;
        this.postalCode = postalCode;
        this.neighborhood = neighborhood;
        this.city = city;
        this.state = state;
    }
}
