package com.integrator.cdf.services;

import com.integrator.cdf.dto.OrderIntegrationPayload;
import com.integrator.cdf.dto.PartnerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PartnerService {

    String URL = "https://webservicehml.cdf.net:8443/ContratosEcommerceApi/pedidos";
    String ACCESS_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3dlYnNlcnZpY2UuY2RmLm5ldCIsInVpZCI6NSwiY2lkIjoiTFJISmlEMk9QeFdhWWV0TUtEWC9Xdz09IiwiaWF0IjoxNjI0MDQ1NDk4LCJleHAiOjE2MjY2Mzc0OTh9.NeWrICg7Hv0_odbNdUoeuRSEIBaNPYqKgfh4BFFHSgY";

    @Autowired
    private RestTemplate restTemplate;

    public PartnerResponse orderIntegration(OrderIntegrationPayload payload){

        PartnerResponse response = null;

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ ACCESS_TOKEN);

            HttpEntity<OrderIntegrationPayload> request = new HttpEntity<>(payload, headers);
            response = restTemplate.exchange(URL, HttpMethod.POST, request, PartnerResponse.class).getBody();
        }catch (Exception e){
            e.printStackTrace();
        }

        return response;
    }
}
