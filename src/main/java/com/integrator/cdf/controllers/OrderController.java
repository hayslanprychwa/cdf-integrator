package com.integrator.cdf.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.integrator.cdf.dto.OrderIntegrationPayload;
import com.integrator.cdf.dto.PartnerResponse;
import com.integrator.cdf.entities.Order;
import com.integrator.cdf.repository.OrderRepository;
import com.integrator.cdf.services.OrderService;
import com.integrator.cdf.services.PartnerService;
import com.integrator.cdf.services.VtexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;
import java.util.UUID;

@RestController()
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private VtexService vtexService;

    @GetMapping()
    public Iterable<Order> findAll() {
        return orderRepository.findAll();
    }

    @GetMapping("/{orderId}")
    public Order findByOrderId(@PathVariable String orderId) {
        return orderRepository.findByOrderId(orderId);
    }

    @GetMapping("/findByOrderSeq/{orderSeq}")
    Order findByOrderSeq(@PathVariable Integer orderSeq) {
        return orderRepository.findByOrderSeq(orderSeq);
    }

    @GetMapping("/findByOrderStatus/{status}")
    List<Order> findByStatus(@PathVariable String status) {
        return orderRepository.findByOrderStatus(status);
    }

    @GetMapping("/findByDocument/{document}")
    List<Order> findByDocument(@PathVariable Long document) {
        return orderRepository.findByDocument(document);
    }

    @GetMapping("/findByEmail/{email}")
    List<Order> findByEmailList(@PathVariable String email) {
        return orderRepository.findByEmailList_Email(email);
    }

    @GetMapping("/findByPhone/{phone}")
    List<Order> findByPhoneList(@PathVariable String phone) {
        return orderRepository.findByPhoneList_Phone(phone);
    }

    @GetMapping("/findByCreationDate/{creationDate}")
    List<Order> findByCreationDate(@PathVariable Date creationDate) {
        return orderRepository.findByCreationDate(creationDate);
    }

    @GetMapping("/findByCreationDate/{startDate}/{endDate}")
    List<Order> findByCreationDateStartAndEnd(@PathVariable Date startDate, @PathVariable Date endDate) {
        return orderRepository.findByCreationDateStartAndEnd(startDate, endDate);
    }

    @PostMapping()
    public Order create(@RequestBody Order order) {
        return orderRepository.save(order);
    }

    @PostMapping("/orderInput")
    public Order insertOrder(@RequestBody JsonNode payload) {
        Order order = orderService.processOrderInput(payload);
        return this.create(order);
    }

    @PostMapping("/orderIntegration")
    public PartnerResponse orderIntegration(@RequestBody OrderIntegrationPayload payload){
        return partnerService.orderIntegration(payload);
    }

    @GetMapping("/orderIntegration/{orderId}")
    public PartnerResponse findOrderIntegrationByOrderId(@PathVariable String orderId){

        PartnerResponse partnerResponse = null;

        try {
            JsonNode bigOrder = vtexService.orderIntegration(orderId);
            Order myOrder = this.insertOrder(bigOrder);
            String customerName = myOrder.getFirstName() + "." + myOrder.getLastName();
            OrderIntegrationPayload partnerPayload = new OrderIntegrationPayload(156, myOrder.getOrderId(), myOrder.getDocument(), "0001", myOrder.getTotals(), myOrder.getCreationDate(), myOrder.getShippingEstimateDate(), myOrder.getShippingEstimateDate(), customerName, myOrder.getItemsName());
            partnerResponse = this.orderIntegration(partnerPayload);

        } catch (Exception e){
            e.printStackTrace();
        }

        return partnerResponse;

    }

    @PutMapping()
    public Order update(@RequestBody Order order) {
        return orderRepository.save(order);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        orderRepository.deleteById(id);
    }

}
