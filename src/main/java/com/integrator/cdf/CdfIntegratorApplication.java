package com.integrator.cdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CdfIntegratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CdfIntegratorApplication.class, args);
    }

}
