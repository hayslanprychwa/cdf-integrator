package com.integrator.cdf.dto;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
public class OrderIntegrationPayload {

    Integer idClienteCorporativo;
    String  nrPedido;
    Long  nrCpfCnpj;
    String  cdProdutoCliente;
    Integer  vlServicoContrato;
    Date dtAdesao;
    Date  dtInicioContrato;
    Date  dtFimContrato;
    String  produtoModelo;
    String  nomeCliente;
    String  dsProdutoServico;
    String  nrSerieSorteio;
    String  nrSorteSorteio;

    public OrderIntegrationPayload(Integer idClienteCorporativo, String nrPedido, Long nrCpfCnpj, String cdProdutoCliente, Integer vlServicoContrato, Date dtAdesao, Date dtInicioContrato, Date dtFimContrato, String nomeCliente, String dsProdutoServico) {
        this.idClienteCorporativo = idClienteCorporativo;
        this.nrPedido = nrPedido;
        this.nrCpfCnpj = nrCpfCnpj;
        this.cdProdutoCliente = cdProdutoCliente;
        this.vlServicoContrato = vlServicoContrato;
        this.dtAdesao = dtAdesao;
        this.dtInicioContrato = dtInicioContrato;
        this.dtFimContrato = dtFimContrato;
        this.nomeCliente = nomeCliente;
        this.dsProdutoServico = dsProdutoServico;
    }
}
