package com.integrator.cdf.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "orders")
@Getter
@Setter
public class Order {

    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    UUID id;

    @Column(unique = true)
    String orderId;

    @Column(unique = true)
    Integer orderSeq;

    @Column
    String orderStatus;

    @Column
    String firstName;

    @Column
    String lastName;

    @Column
    String documentType;

    @Column
    Long document;

    @Column
    Date creationDate;

    @Column
    Date shippingEstimateDate;

    @Column
    Integer totals;

    @Column
    Integer itemsId;

    @Column
    String itemsName;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "emailList",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "emailId"))
    Set<Email> emailList;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "phoneList",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "phoneId"))
    Set<Phone> phoneList;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "addressList",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "addressId"))
    Set<Address> addressList;

    public Order() { }

    public Order( String orderId, Integer orderSeq, String orderStatus, String firstName, String lastName, String documentType, Long document, Date creationDate, Date shippingEstimateDate, Integer totals, Integer itemsId, String itemsName, Set<Email> emailList, Set<Phone> phoneList, Set<Address> addressList) {
        this.orderId = orderId;
        this.orderSeq = orderSeq;
        this.orderStatus = orderStatus;
        this.firstName = firstName;
        this.lastName = lastName;
        this.documentType = documentType;
        this.document = document;
        this.creationDate = creationDate;
        this.shippingEstimateDate = shippingEstimateDate;
        this.totals = totals;
        this.itemsId = itemsId;
        this.itemsName = itemsName;
        this.emailList = emailList;
        this.phoneList = phoneList;
        this.addressList = addressList;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getDocument() {
        return document;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getShippingEstimateDate() {
        return shippingEstimateDate;
    }

    public Integer getTotals() {
        return totals;
    }

    public Integer getItemsId() {
        return itemsId;
    }

    public String getItemsName() {
        return itemsName;
    }
}
