package com.integrator.cdf.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.integrator.cdf.entities.Address;
import com.integrator.cdf.entities.Email;
import com.integrator.cdf.entities.Order;
import com.integrator.cdf.entities.Phone;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Service
public class OrderService {

    public Order processOrderInput(JsonNode payload){
        String orderId = payload.get("orderId").asText();
        Integer orderSeq = payload.get("sequence").asInt();
        String orderStatus = payload.get("status").asText();
        String firstName = payload.at("/clientProfileData/firstName").asText();
        String lastName = payload.at("/clientProfileData/lastName").asText();
        String documentType = payload.at("/clientProfileData/documentType").asText();
        Long document = payload.at("/clientProfileData/document").asLong();
        Long creationDate = payload.get("creationDate").asLong();
        Long shippingEstimateDate = payload.at("/shippingData/logisticsInfo").get(0).get("shippingEstimateDate")
                .asLong();
        Integer totals = payload.get("value").asInt();
        Integer itemsId = payload.get("items").get(0).get("id").asInt();
        String itemsName = payload.get("items").get(0).get("name").asText();
        String email = payload.at("/clientProfileData/email").asText();
        String phone = payload.at("/clientProfileData/phone").asText();
        JsonNode address = payload.at("/shippingData/selectedAddresses");

        Set<Email> emailList = new HashSet<>();
        emailList.add(new Email(email));

        Set<Phone> phoneList = new HashSet<>();
        Integer areaCode = null;
        String phoneNumber = phone;
        Integer NR_RAMAL = null;
        String receiverName = address.get(0).get("receiverName").asText();
        phoneList.add(new Phone(areaCode, phoneNumber, NR_RAMAL, receiverName));

        Set<Address> addressList = new HashSet<>();
        String street = address.get(0).get("street").asText();
        Integer number = address.get(0).get("number").asInt();
        Integer postalCode = address.get(0).get("postalCode").asInt();
        String neighborhood = address.get(0).get("neighborhood").asText();
        String city = address.get(0).get("city").asText();
        String state = address.get(0).get("state").asText();
        addressList.add(new Address(street, number, postalCode, neighborhood, city, state));

        Order order = new Order(orderId, orderSeq, orderStatus, firstName, lastName, documentType, document,
                new Date(creationDate), new Date(shippingEstimateDate), totals, itemsId, itemsName, emailList,
                phoneList, addressList);

        return order;
    }
}
