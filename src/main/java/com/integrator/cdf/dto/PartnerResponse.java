package com.integrator.cdf.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartnerResponse {

    String message;
    String status;
    Integer idClienteContrato;
    Integer idClienteCorporativo;
    String nrPedido;

}
